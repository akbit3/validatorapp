DROP TABLE IF EXISTS TBL_MT300;

CREATE TABLE tbl_mt300 (
  ID int NOT NULL AUTO_INCREMENT,
  FILE_ID int NOT NULL,
  Sender varchar(30) DEFAULT NULL,
  Receiver varchar(30) DEFAULT NULL,
  15A_F_20 varchar(30) DEFAULT NULL,
15A_F_21 varchar(30) DEFAULT NULL,
15A_F_22A varchar(30) DEFAULT NULL,
15A_F_94A varchar(30) DEFAULT NULL,
15A_F_22C varchar(30) DEFAULT NULL,
15A_F_17T varchar(30) DEFAULT NULL,
15A_F_17U varchar(30) DEFAULT NULL,
15A_F_17I varchar(30) DEFAULT NULL,
15A_F_82A varchar(30) DEFAULT NULL,
15A_F_82D varchar(30) DEFAULT NULL,
15A_F_82J varchar(30) DEFAULT NULL,
15A_F_87A varchar(30) DEFAULT NULL,
15A_F_87D varchar(30) DEFAULT NULL,
15A_F_87J varchar(30) DEFAULT NULL,
15A_F_83A varchar(30) DEFAULT NULL,
15A_F_83D varchar(30) DEFAULT NULL,
15A_F_83J varchar(30) DEFAULT NULL,
15A_F_77D varchar(30) DEFAULT NULL,
15A_F_77H varchar(30) DEFAULT NULL,
15A_F_14C varchar(30) DEFAULT NULL,
15A_F_17F varchar(30) DEFAULT NULL,
15B_F_30T varchar(30) DEFAULT NULL,
15B_F_30V varchar(30) DEFAULT NULL,
15B_F_36 varchar(30) DEFAULT NULL,
15B_B1_F_32B varchar(30) DEFAULT NULL,
15B_B1_F_53A varchar(30) DEFAULT NULL,
15B_B1_F_53D varchar(30) DEFAULT NULL,
15B_B1_F_53J varchar(30) DEFAULT NULL,
15B_B1_F_56A varchar(30) DEFAULT NULL,
15B_B1_F_56D varchar(30) DEFAULT NULL,
15B_B1_F_56J varchar(30) DEFAULT NULL,
15B_B1_F_57A varchar(30) DEFAULT NULL,
15B_B1_F_57D varchar(30) DEFAULT NULL,
15B_B1_F_57J varchar(30) DEFAULT NULL,
15B_B2_F_33B varchar(30) DEFAULT NULL,
15B_B2_F_53A varchar(30) DEFAULT NULL,
15B_B2_F_53D varchar(30) DEFAULT NULL,
15B_B2_F_53J varchar(30) DEFAULT NULL,
15B_B2_F_56A varchar(30) DEFAULT NULL,
15B_B2_F_56D varchar(30) DEFAULT NULL,
15B_B2_F_56J varchar(30) DEFAULT NULL,
15B_B2_F_57A varchar(30) DEFAULT NULL,
15B_B2_F_57D varchar(30) DEFAULT NULL,
15B_B2_F_57J varchar(30) DEFAULT NULL,
15B_B2_F_58A varchar(30) DEFAULT NULL,
15B_B2_F_58D varchar(30) DEFAULT NULL,
15B_B2_F_58J varchar(30) DEFAULT NULL,
15C_F_29A varchar(30) DEFAULT NULL,
15C_F_24D varchar(30) DEFAULT NULL,
15C_F_84A varchar(30) DEFAULT NULL,
15C_F_84B varchar(30) DEFAULT NULL,
15C_F_84D varchar(30) DEFAULT NULL,
15C_F_84J varchar(30) DEFAULT NULL,
15C_F_85A varchar(30) DEFAULT NULL,
15C_F_85B varchar(30) DEFAULT NULL,
15C_F_85D varchar(30) DEFAULT NULL,
15C_F_85J varchar(30) DEFAULT NULL,
15C_F_88A varchar(30) DEFAULT NULL,
15C_F_88D varchar(30) DEFAULT NULL,
15C_F_88J varchar(30) DEFAULT NULL,
15C_F_71F varchar(30) DEFAULT NULL,
15C_F_26H varchar(30) DEFAULT NULL,
15C_F_21G varchar(30) DEFAULT NULL,
15C_F_72 varchar(30) DEFAULT NULL,
15D_F_17A varchar(30) DEFAULT NULL,
15D_F_32B varchar(30) DEFAULT NULL,
15D_F_53A varchar(30) DEFAULT NULL,
15D_F_53D varchar(30) DEFAULT NULL,
15D_F_53J varchar(30) DEFAULT NULL,
15D_F_56A varchar(30) DEFAULT NULL,
15D_F_56D varchar(30) DEFAULT NULL,
15D_F_56J varchar(30) DEFAULT NULL,
15D_F_57A varchar(30) DEFAULT NULL,
15D_F_57D varchar(30) DEFAULT NULL,
15D_F_57J varchar(30) DEFAULT NULL,
15D_F_58A varchar(30) DEFAULT NULL,
15D_F_58D varchar(30) DEFAULT NULL,
15D_F_58J varchar(30) DEFAULT NULL,
15D_F_16A varchar(30) DEFAULT NULL,
15E_F_81A varchar(30) DEFAULT NULL,
15E_F_81D varchar(30) DEFAULT NULL,
15E_F_81J varchar(30) DEFAULT NULL,
15E_F_89A varchar(30) DEFAULT NULL,
15E_F_89D varchar(30) DEFAULT NULL,
15E_F_89J varchar(30) DEFAULT NULL,
15E_F_96A varchar(30) DEFAULT NULL,
15E_F_96D varchar(30) DEFAULT NULL,
15E_F_96J varchar(30) DEFAULT NULL,
15E_F_22S varchar(30) DEFAULT NULL,
15E_F_22T varchar(30) DEFAULT NULL,
15E_F_17E varchar(30) DEFAULT NULL,
15E_F_22U varchar(30) DEFAULT NULL,
15E_F_17H varchar(30) DEFAULT NULL,
15E_F_17P varchar(30) DEFAULT NULL,
15E_F_35B varchar(30) DEFAULT NULL,
15E_F_22V varchar(30) DEFAULT NULL,
15E_F_98D varchar(30) DEFAULT NULL,
15E_F_17W varchar(30) DEFAULT NULL,
15E_F_22W varchar(30) DEFAULT NULL,
15E_F_17Y varchar(30) DEFAULT NULL,
15E_F_17Z varchar(30) DEFAULT NULL,
15E_F_22Q varchar(30) DEFAULT NULL,
15E_F_17L varchar(30) DEFAULT NULL,
15E_F_17M varchar(30) DEFAULT NULL,
15E_F_17Q varchar(30) DEFAULT NULL,
15E_F_17S varchar(30) DEFAULT NULL,
15E_F_17X varchar(30) DEFAULT NULL,
15E_F_98G varchar(30) DEFAULT NULL,
15E_F_98H varchar(30) DEFAULT NULL,
15E_F_77A varchar(30) DEFAULT NULL,
15E_E1_F_22L varchar(30) DEFAULT NULL,
15E_E1_F_91A varchar(30) DEFAULT NULL,
15E_E1_F_91D varchar(30) DEFAULT NULL,
15E_E1_F_91J varchar(30) DEFAULT NULL,
15E_E1a_F_22M varchar(30) DEFAULT NULL,
15E_E1a_F_22N varchar(30) DEFAULT NULL,
15E_E1a_A1_F_22P varchar(30) DEFAULT NULL,
15E_E1a_A1_F_22R varchar(30) DEFAULT NULL,

  File_Creation_Date datetime DEFAULT NULL,
  CREATED datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (ID)
) ENGINE=InnoDB;