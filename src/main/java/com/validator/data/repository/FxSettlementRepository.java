package com.validator.data.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class FxSettlementRepository implements IValidatorRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Value("${conf.settlement.fx.fields.index}")
	private String fxSettlementIndex;

	@Value("${conf.settlement.fx.fields}")
	private String fxSettlementFields;

    class FileRowMapper implements RowMapper<Map<String, Object>> {
		@Override
		public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
			Map<String, Object> settlementMap = new HashMap<String, Object>();
			settlementMap.put("Id", rs.getLong("id"));
			settlementMap.put("file_id", rs.getLong("file_id"));

//			tradeMap.put("field1", rs.getString("field1"));
			String[] fields = fxSettlementFields.split(",");
			for (int i = 0; i < fields.length; i++) {
				settlementMap.put(fields[i], rs.getString(fields[i]));
			}

			settlementMap.put("created", rs.getDate("created"));
			settlementMap.put("modified", rs.getDate("modified"));

			return settlementMap;
		}
	}

	public List<Map<String, Object>> findAll() {
		return jdbcTemplate.query("select * from tbl_settlement_fx", new FileRowMapper());
	}

	@SuppressWarnings("rawtypes")
	public Optional<Map> findByFileDate(Long fileId) {
		return Optional.of(jdbcTemplate.queryForObject("select * from tbl_settlement_fx where file_id=?",
				new Object[] { fileId }, new BeanPropertyRowMapper<HashMap>(HashMap.class)));
	}

	public int deleteByFileID(Long fileId) {
		return jdbcTemplate.update("delete from tbl_settlement_fx where file_id=?", new Object[] {fileId});
	}

	@Override
	public int insert(Map<String, Object> settlementMap) {

		// build insert query string and value object
		String insertQueryString = "insert into tbl_settlement_fx (file_id, " + fxSettlementFields + ", created ) values(?, ?";
		String[] fields = fxSettlementFields.split(",");
		Object[] valObj = new Object[fields.length + 2];

		valObj[0] = settlementMap.get("file_id");
		

		for (int i = 0; i < fields.length; i++) {
			insertQueryString += ",?";
			valObj[i + 1] = settlementMap.get(fields[i]);
		}
		insertQueryString += ")";
		valObj[fields.length + 1] = settlementMap.get("created");

		return jdbcTemplate.update(insertQueryString, valObj);
	}

	public int update(Map<String, Object> settlementMap) {
		/*return jdbcTemplate.update(
				"update tbl_settlement_fx " + " set file_id = ?, field1 = ?, field2 = ?, field3 = ?, field4 = ?, created = ? "
						+ " where id = ?",
				new Object[] { tradeFx.getFileId(), tradeFx.getField1(), tradeFx.getField2(), tradeFx.getField3(),
						tradeFx.getField4(), tradeFx.getCreated(), tradeFx.getId() });*/
		// TODO
		// Implement the update similar to insert.
		return 0;
		
	}

	public Optional<List<Map<String, Object>>> findByField(String fieldName, String fieldValue) {
		return Optional.of(jdbcTemplate.queryForList("select * from tbl_trd_fx where "+fieldName+" =?",
				new Object[] {fieldValue}));
	}
}