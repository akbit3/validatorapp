package com.validator.data.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.validator.data.model.FileAudit;

@Repository
public class FileRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    class FileRowMapper implements RowMapper <FileAudit> {
        @Override
        public FileAudit mapRow(ResultSet rs, int rowNum) throws SQLException {
            FileAudit file = new FileAudit();
            file.setId(rs.getLong("id"));
            file.setFileName(rs.getString("file_name"));
            file.setFileDate(rs.getString("file_date"));
            file.setFileType(rs.getString("file_type"));
            file.setFileRegion(rs.getString("file_region"));
            file.setCreated(rs.getDate("created"));
            file.setModified(rs.getDate("modified"));
            return file;
        }
    }

    public List <FileAudit> findAll() {
        return jdbcTemplate.query("select * from tbl_bdt_files", new FileRowMapper());
    }
    
    public Optional<List<FileAudit>> findByFileType(String fileType) {
        return Optional.of(jdbcTemplate.query("select * from tbl_bdt_files where file_type=?", new Object[] {fileType}, new BeanPropertyRowMapper <FileAudit> (FileAudit.class)));
    }

    public Optional <FileAudit> findByFileDate(String fileDate) {
        return Optional.of(jdbcTemplate.queryForObject("select * from tbl_bdt_files where file_date=?", new Object[] {
        		fileDate
            },
            new BeanPropertyRowMapper <FileAudit> (FileAudit.class)));
    }
    
    public Optional <FileAudit> findByFileId(String fileId) {
    	Long id = Long.valueOf(fileId);
        return Optional.of((FileAudit) jdbcTemplate.queryForObject("select * from tbl_bdt_files where id=?", new Object[] {
        		id
            },
            new BeanPropertyRowMapper <FileAudit> (FileAudit.class)));
    }

    public int deleteByFileDate(String fileDate) {
        return jdbcTemplate.update("delete from tbl_bdt_files where file_date=?", new Object[] {
        		fileDate
        });
    }
    
    public long getMaxId() {
    	return jdbcTemplate.queryForObject("select max(id) from tbl_bdt_files", Long.class);
    }

    public long insert(FileAudit file) {
    	return jdbcTemplate.update("insert into tbl_bdt_files (file_name, file_date, file_type, file_region, created ) " + "values(?, ?, ?, ?, ?)",
            new Object[] {
                file.getFileName(), file.getFileDate(), file.getFileType(), file.getFileRegion(), file.getCreated()
            });
        
    }

    public int update(FileAudit file) {
        return jdbcTemplate.update("update tbl_bdt_files " + " set file_name = ?, file_date = ?, file_type = ?, file_region = ?, created = ? " + " where id = ?",
            new Object[] {
                file.getFileName(), file.getFileDate(), file.getFileType(), file.getFileRegion(), file.getCreated(), file.getId()
            });
    }
}