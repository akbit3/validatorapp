package com.validator.data.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.validator.data.model.EventFx;

@Repository
public class EventFxRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    class FileRowMapper implements RowMapper <EventFx> {
        @Override
        public EventFx mapRow(ResultSet rs, int rowNum) throws SQLException {
        	EventFx eventFx = new EventFx();
        	eventFx.setId(rs.getLong("id"));
        	eventFx.setFileId(rs.getLong("file_id"));
        	eventFx.setField1(rs.getString("field1"));
        	eventFx.setField2(rs.getString("field2"));
        	eventFx.setField3(rs.getString("field3"));
        	eventFx.setField4(rs.getString("field4"));
        	eventFx.setCreated(rs.getDate("created"));
        	eventFx.setModified(rs.getDate("modified"));
            return eventFx;
        }
    }

    public List <EventFx> findAll() {
        return jdbcTemplate.query("select * from tbl_evnt_fx", new FileRowMapper());
    }

    public Optional <EventFx> findByFileDate(Long fileId) {
        return Optional.of(jdbcTemplate.queryForObject("select * from tbl_evnt_fx where file_id=?", new Object[] {
        		fileId
            },
            new BeanPropertyRowMapper <EventFx> (EventFx.class)));
    }

    public int deleteByFileID(Long fileId) {
        return jdbcTemplate.update("delete from tbl_evnt_fx where file_id=?", new Object[] {
        		fileId
        });
    }

    public int insert(EventFx eventFx) {
        return jdbcTemplate.update("insert into tbl_evnt_fx (file_id, field1, field2, field3, field4, created ) " + "values(?, ?, ?, ?, ?, ?)",
            new Object[] {
            		eventFx.getFileId(), eventFx.getField1(), eventFx.getField2(), eventFx.getField3(), eventFx.getField4(), eventFx.getCreated()
            });
    }

    public int update(EventFx eventFx) {
        return jdbcTemplate.update("update tbl_trd_fx " + " set file_id = ?, field1 = ?, field2 = ?, field3 = ?, field4 = ?, created = ? " + " where id = ?",
            new Object[] {
            		eventFx.getFileId(), eventFx.getField1(), eventFx.getField2(), eventFx.getField3(), eventFx.getField4(), eventFx.getCreated(), eventFx.getId()
            });
    }
}