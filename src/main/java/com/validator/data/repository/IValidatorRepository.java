package com.validator.data.repository;

import java.util.Map;

public interface IValidatorRepository {
	int insert(Map<String, Object> tradeMap);
}
