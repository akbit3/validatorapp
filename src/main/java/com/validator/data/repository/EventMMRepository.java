package com.validator.data.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.validator.data.model.EventMM;

@Repository
public class EventMMRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    class FileRowMapper implements RowMapper <EventMM> {
        @Override
        public EventMM mapRow(ResultSet rs, int rowNum) throws SQLException {
        	EventMM eventMM = new EventMM();
        	eventMM.setId(rs.getLong("id"));
        	eventMM.setFileId(rs.getLong("file_id"));
        	eventMM.setField1(rs.getString("field1"));
        	eventMM.setField2(rs.getString("field2"));
        	eventMM.setField3(rs.getString("field3"));
        	eventMM.setField4(rs.getString("field4"));
        	eventMM.setCreated(rs.getDate("created"));
        	eventMM.setModified(rs.getDate("modified"));
            return eventMM;
        }
    }

    public List <EventMM> findAll() {
        return jdbcTemplate.query("select * from tbl_evnt_mm", new FileRowMapper());
    }

    public Optional <EventMM> findByFileDate(Long fileId) {
        return Optional.of(jdbcTemplate.queryForObject("select * from tbl_evnt_mm where file_id=?", new Object[] {
        		fileId
            },
            new BeanPropertyRowMapper <EventMM> (EventMM.class)));
    }

    public int deleteByFileID(Long fileId) {
        return jdbcTemplate.update("delete from tbl_evnt_mm where file_id=?", new Object[] {
        		fileId
        });
    }

    public int insert(EventMM eventMM) {
        return jdbcTemplate.update("insert into tbl_evnt_mm (file_id, field1, field2, field3, field4, created ) " + "values(?, ?, ?, ?, ?, ?)",
            new Object[] {
            		eventMM.getFileId(), eventMM.getField1(), eventMM.getField2(), eventMM.getField3(), eventMM.getField4(), eventMM.getCreated()
            });
    }

    public int update(EventMM eventMM) {
        return jdbcTemplate.update("update tbl_trd_fx " + " set file_id = ?, field1 = ?, field2 = ?, field3 = ?, field4 = ?, created = ? " + " where id = ?",
            new Object[] {
            		eventMM.getFileId(), eventMM.getField1(), eventMM.getField2(), eventMM.getField3(), eventMM.getField4(), eventMM.getCreated(), eventMM.getId()
            });
    }
}