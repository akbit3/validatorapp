package com.validator.data.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.validator.data.model.TradeMM;

@Repository
public class TradeMMRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    class FileRowMapper implements RowMapper <TradeMM> {
        @Override
        public TradeMM mapRow(ResultSet rs, int rowNum) throws SQLException {
        	TradeMM tradeMM = new TradeMM();
        	tradeMM.setId(rs.getLong("id"));
        	tradeMM.setFileId(rs.getLong("file_id"));
        	tradeMM.setField1(rs.getString("field1"));
        	tradeMM.setField2(rs.getString("field2"));
        	tradeMM.setField3(rs.getString("field3"));
        	tradeMM.setField4(rs.getString("field4"));
        	tradeMM.setCreated(rs.getDate("created"));
        	tradeMM.setModified(rs.getDate("modified"));
            return tradeMM;
        }
    }

    public List <TradeMM> findAll() {
        return jdbcTemplate.query("select * from tbl_trd_mm", new FileRowMapper());
    }

    public Optional <TradeMM> findByFileDate(Long fileId) {
        return Optional.of(jdbcTemplate.queryForObject("select * from tbl_trd_mm where file_id=?", new Object[] {
        		fileId
            },
            new BeanPropertyRowMapper <TradeMM> (TradeMM.class)));
    }

    public int deleteByFileID(Long fileId) {
        return jdbcTemplate.update("delete from tbl_trd_mm where file_id=?", new Object[] {
        		fileId
        });
    }

    public int insert(TradeMM tradeMM) {
        return jdbcTemplate.update("insert into tbl_trd_mm (file_id, field1, field2, field3, field4, created ) " + "values(?, ?, ?, ?, ?, ?)",
            new Object[] {
            		tradeMM.getFileId(), tradeMM.getField1(), tradeMM.getField2(), tradeMM.getField3(), tradeMM.getField4(), tradeMM.getCreated()
            });
    }

    public int update(TradeMM tradeMM) {
        return jdbcTemplate.update("update tbl_trd_mm " + " set file_id = ?, field1 = ?, field2 = ?, field3 = ?, field4 = ?, created = ? " + " where id = ?",
            new Object[] {
            		tradeMM.getFileId(), tradeMM.getField1(), tradeMM.getField2(), tradeMM.getField3(), tradeMM.getField4(), tradeMM.getCreated(), tradeMM.getId()
            });
    }
}