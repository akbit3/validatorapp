package com.validator.data.repository;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class Swift300Repository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	class FileRowMapper implements RowMapper<Map<String, Object>> {
		@Override
		public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
			Map<String, Object> swiftMap = new HashMap<String, Object>();
			ResultSetMetaData rsMetaData = rs.getMetaData();
			
			for (int i = 0; i < rsMetaData.getColumnCount(); i++) {
				swiftMap.put(rsMetaData.getColumnName(i+1), rs.getObject(rsMetaData.getColumnName(i+1)));
			}

			return swiftMap;
		}
	}

	public List<Map<String, Object>> findAll() {
		return jdbcTemplate.query("select * from tbl_trd_fx", new FileRowMapper());
	}

	@SuppressWarnings("rawtypes")
	public Optional<Map> findByFileDate(Long fileId) {
		return Optional.of(jdbcTemplate.queryForObject("select * from tbl_trd_fx where file_id=?",
				new Object[] { fileId }, new BeanPropertyRowMapper<HashMap>(HashMap.class)));
	}

	public int deleteByFileID(Long fileId) {
		return jdbcTemplate.update("delete from tbl_trd_fx where file_id=?", new Object[] { fileId });
	}

	public int insert(Long fileId, Map<String, String> swiftmap) {

		// build insert query string and value object
		String swiftFields="", swiftParams="";
		
		Object[] valObj = new Object[swiftmap.size() + 1];
		valObj[0] = fileId;
		int i=0;
		for(Entry<String, String> fldEntry : swiftmap.entrySet()){
			swiftFields= i==0 ? fldEntry.getKey() : swiftFields+","+fldEntry.getKey();
			swiftParams= i==0 ? "?" : swiftParams+","+"?";
			valObj[i+1] = fldEntry.getValue();
			i++;
		}

		String insertQueryString = "insert into tbl_mt300 (file_id, " + swiftFields + " ) values(?, "+swiftParams+")";
		System.out.println(insertQueryString);
		return jdbcTemplate.update(insertQueryString, valObj);
	}

	public int update(Map<String, Object> tradeMap) {
		/*return jdbcTemplate.update(
				"update tbl_trd_fx " + " set file_id = ?, field1 = ?, field2 = ?, field3 = ?, field4 = ?, created = ? "
						+ " where id = ?",
				new Object[] { tradeFx.getFileId(), tradeFx.getField1(), tradeFx.getField2(), tradeFx.getField3(),
						tradeFx.getField4(), tradeFx.getCreated(), tradeFx.getId() });*/
		
		// TODO
		// Implement update similar to insert.
		return 0;
	}
}