package com.validator.data.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class TradeFxRepository implements IValidatorRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Value("${conf.trd.fx.fields.index}")
	private String fxTradeIndex;

	@Value("${conf.trd.fx.fields}")
	private String fxTradeFields;

	class FileRowMapper implements RowMapper<Map<String, Object>> {
		@Override
		public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
			Map<String, Object> tradeMap = new HashMap<String, Object>();
			tradeMap.put("Id", rs.getLong("id"));
			tradeMap.put("file_id", rs.getLong("file_id"));

//			tradeMap.put("field1", rs.getString("field1"));
			String[] fields = fxTradeFields.split(",");
			for (int i = 0; i < fields.length; i++) {
				tradeMap.put(fields[i], rs.getString(fields[i]));
			}

			tradeMap.put("created", rs.getDate("created"));
			tradeMap.put("modified", rs.getDate("modified"));

			return tradeMap;
		}
	}

	public List<Map<String, Object>> findAll() {
		return jdbcTemplate.query("select * from tbl_trd_fx", new FileRowMapper());
	}

	@SuppressWarnings("rawtypes")
	public Optional<Map> findByFileDate(Long fileId) {
		return Optional.of(jdbcTemplate.queryForObject("select * from tbl_trd_fx where file_id=?",
				new Object[] { fileId }, new BeanPropertyRowMapper<HashMap>(HashMap.class)));
	}
	
	public Optional<List<Map<String, Object>>> findByField(String fieldName, String fieldValue) {
		return Optional.of(jdbcTemplate.queryForList("select * from tbl_trd_fx where "+fieldName+" =?",
				new Object[] {fieldValue}));
	}

	public int deleteByFileID(Long fileId) {
		return jdbcTemplate.update("delete from tbl_trd_fx where file_id=?", new Object[] { fileId });
	}

	@Override
	public int insert(Map<String, Object> tradeMap) {

		// build insert query string and value object
		String insertQueryString = "insert into tbl_trd_fx (file_id, " + fxTradeFields + ", created ) values(?, ?";
		String[] fields = fxTradeFields.split(",");
		Object[] valObj = new Object[fields.length + 2];

		valObj[0] = tradeMap.get("file_id");
		

		for (int i = 0; i < fields.length; i++) {
			insertQueryString += ",?";
			valObj[i + 1] = tradeMap.get(fields[i]);
		}
		insertQueryString += ")";
		valObj[fields.length + 1] = tradeMap.get("created");

		return jdbcTemplate.update(insertQueryString, valObj);
	}

	public int update(Map<String, Object> tradeMap) {
		/*return jdbcTemplate.update(
				"update tbl_trd_fx " + " set file_id = ?, field1 = ?, field2 = ?, field3 = ?, field4 = ?, created = ? "
						+ " where id = ?",
				new Object[] { tradeFx.getFileId(), tradeFx.getField1(), tradeFx.getField2(), tradeFx.getField3(),
						tradeFx.getField4(), tradeFx.getCreated(), tradeFx.getId() });*/
		
		// TODO
		// Implement update similar to insert.
		return 0;
	}
}