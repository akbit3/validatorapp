package com.validator.data.model;

import java.util.Date;

public class FileAudit {

    private long id;
    private String fileName;
    private String fileDate;
    private String fileType;
    private String fileRegion;
    private Date created;
    private Date modified;
    
    
    public FileAudit(String fileName, String fileDate, String fileType, String fileRegion) {
		super();
		this.fileName = fileName;
		this.fileDate = fileDate;
		this.fileType = fileType;
		this.fileRegion = fileRegion;
		this.created = new Date();
	}


	public FileAudit() {
		this.created = new Date();
    }


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFileDate() {
		return fileDate;
	}


	public void setFileDate(String fileDate) {
		this.fileDate = fileDate;
	}


	public String getFileType() {
		return fileType;
	}


	public void setFileType(String fileType) {
		this.fileType = fileType;
	}


	public String getFileRegion() {
		return fileRegion;
	}


	public void setFileRegion(String fileRegion) {
		this.fileRegion = fileRegion;
	}


	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}


	public Date getModified() {
		return modified;
	}


	public void setModified(Date modified) {
		this.modified = modified;
	}

 }
