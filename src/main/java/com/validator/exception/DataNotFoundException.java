package com.validator.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DataNotFoundException extends RuntimeException {

	
	private static final long serialVersionUID = -8213810471577494930L;

	public DataNotFoundException(String msg) {
		super("Data not found " + msg + "'.");
	}
}
