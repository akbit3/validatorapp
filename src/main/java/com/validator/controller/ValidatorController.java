package com.validator.controller;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.validator.data.model.FileAudit;
import com.validator.service.IValidatorService;

@RestController
//@RequestMapping("/validator")
//@CrossOrigin(origins = "http://127.0.0.1:5500")
public class ValidatorController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IValidatorService validService;

	@GetMapping(value = "/inputbdt")
	public String[] getAllInputBDTs(@RequestParam String fileType) throws FileNotFoundException {
		logger.info("Get all input requested for fileType: "+fileType); 
		// find all the input files from dir
		return validService.findInputFiles(fileType);
	}
	
	@GetMapping(value = "/loadedbdt")
	public List<FileAudit> getAllLoadedBDTs(@RequestParam String fileType) {
		logger.info("Get all loaded requested for fileType: "+fileType); 
		// find all the loaded files from DB
		return validService.findAll(fileType);
	}
	
	@GetMapping(value = "/inputswift")
	public String[] getAllInputSwifts(@RequestParam String fileType) throws FileNotFoundException {
		logger.info("Get all input requested for fileType: "+fileType); 
		// find all the input files from dir
		return validService.findInputFiles(fileType);
	}
	
	@GetMapping(value = "/loadedswift")
	public List<FileAudit> getAllLoadedSwifts(@RequestParam String fileType) {
		logger.info("Get all loaded requested for fileType: "+fileType); 
		// find all the loaded files from DB
		return validService.findAll(fileType);
	}
	
	@PostMapping(value = "/bdt")
	public String[] loadBDTFile(@RequestParam String fileType){
		logger.info("Load requested for fileType: "+fileType); 
		try {
			return validService.loadBDTFiles(fileType);
		} catch (FileNotFoundException e) {
			return null;
		}
	}
	
	@PutMapping(value = "/swift")
	public String[] loadSwiftFiles(@RequestParam String fileType){
		logger.info("Load requested for fileType: "+fileType); 
		try {
			return validService.loadSwiftFiles(fileType);
		} catch (FileNotFoundException e) {
			return null;
		}
	}
	
	@GetMapping(value = "/test")
	public List<Map<String, String[]>> test(){
		logger.info("Test fired"); 
		
		List<Map<String, String[]>> resultList = new ArrayList<>();
		
		Map<String, String[]> fieldMap1 = new HashMap<>();
		fieldMap1.put("field11", new String[] {"field11Value", "Y"});
		fieldMap1.put("field12", new String[] {"field12Value", "N"});
		
		Map<String, String[]> fieldMap2 = new HashMap<>();
		fieldMap2.put("field21", new String[] {"field21Value", "Y"});
		fieldMap2.put("field22", new String[] {"field22Value", "N"});
		
		resultList.add(fieldMap1);
		resultList.add(fieldMap2);
		
			return resultList;
	}
}
