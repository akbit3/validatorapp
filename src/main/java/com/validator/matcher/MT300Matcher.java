package com.validator.matcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.validator.data.model.FileAudit;
import com.validator.data.repository.FileRepository;
import com.validator.data.repository.FxSettlementRepository;
import com.validator.data.repository.TradeFxRepository;

@Service
public class MT300Matcher {

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private TradeFxRepository tradeFxRepository;

	@Autowired
	private FxSettlementRepository settlementFxRepository;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private List<String> mandatoryBDTMatch = Arrays.asList(new String[] { "TP_FXSWLEG" });

	public List<Map<String, Object>> match(Map<String, String> swiftMap, String swiftFilename) {
		String contractNr = swiftMap.get("15A_F_20");
		String[] contractDetail = contractNr.split("-");

		String swapLeg = "";

		for (int i = 1; i < contractDetail.length; i++) {
			if (contractDetail[i].contains("N")) {
				swapLeg = "nearLeg";
			} else if (contractDetail[i].contains("F")) {
				swapLeg = "farLeg";
			}
		}

		// Get the trade list based on contact nr.
		Optional<List<Map<String, Object>>> fxTradeOpt = tradeFxRepository.findByField("CNT_ORG", contractDetail[0]);
		List<Map<String, Object>> fxTradeList = fxTradeOpt.get();
		
		logger.info("Mapping found for file: "+swiftFilename+", contract: "+contractNr);

		// Add file audit details
		for (Map<String, Object> fxTradeMap : fxTradeList) {
			Optional<FileAudit> fileAuditOpt = fileRepository.findByFileId(fxTradeMap.get("file_id").toString());
			FileAudit fileAudit = fileAuditOpt.get();
			fxTradeMap.put("fileName", fileAudit.getFileName());
		}

		// change data structure to Map<String, String[value, isMatched]> - initially no match.
		List<Map<String, String[]>> newFxTradeList = new ArrayList<>();
		for (Map<String, Object> fxTradeMap : fxTradeList) {
			Map<String, String[]> newFxTradeMap = new HashMap<String, String[]>();
			for(Entry<String, Object> fxTradeEntry : fxTradeMap.entrySet()){
				if(fxTradeEntry.getValue() != null){
					newFxTradeMap.put(fxTradeEntry.getKey(), new String[] {fxTradeEntry.getValue().toString(),"N"});
				} else {
					newFxTradeMap.put(fxTradeEntry.getKey(), new String[] {null,"N"});
				}
			}
			newFxTradeList.add(newFxTradeMap);
		}
		
		
		// drop the heavy object from memory
		fxTradeList=null;

		/*
		 * String valueDate = swiftMap.get("15B_F_30V");
		 * Optional<List<Map<String, Object>>> fxSettleOpt =
		 * settlementFxRepository.findByField("DF_TRDREF", contractDetail[0]);
		 * List<Map<String, Object>> fxSettleList = fxSettleOpt.get();
		 */

		// 1. match swap leg
		if (!swapLeg.equals("")) {
			newFxTradeList = matchTradeField(newFxTradeList, "TP_FXSWLEG", swapLeg);
		}

		// 2. match event -- need to consider the event mapping
		if (swiftMap.keySet().contains("15A_F_22A")) {
			newFxTradeList = matchTradeField(newFxTradeList, "CNT_EVTL", swiftMap.get("15A_F_22A"));
		}

		// 3. match block trade
		if (swiftMap.keySet().contains("15A_F_17T")) {
			newFxTradeList = matchTradeField(newFxTradeList, "BLOCK_TRD", swiftMap.get("15A_F_17T"));
		}

		// 4. match is_NDF
		if (swiftMap.keySet().contains("15A_F_17F")) {
			newFxTradeList = matchTradeField(newFxTradeList, "TP_DVCS", swiftMap.get("15A_F_17F"));
		}

		// 5. match transaction date
		if (swiftMap.keySet().contains("15B_F_30T")) {
			newFxTradeList = matchTradeField(newFxTradeList, "TP_DTETRN", swiftMap.get("15B_F_30T"));
		}

		// 6. match exchange rate
		if (swiftMap.keySet().contains("15B_F_36")) {
			newFxTradeList = matchTradeField(newFxTradeList, "TP_PRICED", swiftMap.get("15B_F_36"));
		}

		// 7. match trader location
		if (swiftMap.keySet().contains("15C_F_84D")) {
			newFxTradeList = matchTradeField(newFxTradeList, "TR_LOCA", swiftMap.get("15C_F_84D"));
		}

		// 8. match ISIN/CFI code
		if (swiftMap.keySet().contains("15E_F_35B")) {
			newFxTradeList = matchTradeField(newFxTradeList, "ISIN", swiftMap.get("15E_F_35B"));
		}

		// 9. match execution venue
		if (swiftMap.keySet().contains("15E_F_22V")) {
			newFxTradeList = matchTradeField(newFxTradeList, "EXEC_MIC", swiftMap.get("15E_F_22V"));
		}

		// 10. match execution time stamp
		if (swiftMap.keySet().contains("15E_F_98D")) {
			newFxTradeList = matchTradeField(newFxTradeList, "EXEC_TS", swiftMap.get("15E_F_98D"));
		}

		// 11. match jurisdiction
		if (swiftMap.keySet().contains("15E_E1_F_22L")) {
			newFxTradeList = matchTradeField(newFxTradeList, "JURISDICT", swiftMap.get("15E_E1_F_22L"));
		}

		// 12. match UTI NS
		if (swiftMap.keySet().contains("15E_E1a_F_22M")) {
			newFxTradeList = matchTradeField(newFxTradeList, "UTI_NS", swiftMap.get("15E_E1a_F_22M"));
		}

		// 13. match UTI
		if (swiftMap.keySet().contains("15E_E1a_F_22N")) {
			newFxTradeList = matchTradeField(newFxTradeList, "UTI_ID", swiftMap.get("15E_E1a_F_22N"));
		}

		// print the output
		for (Map<String, String[]> fxTradeMap : newFxTradeList) {
			fxTradeMap.forEach(
					(k, v) -> System.out.println(k + " = " + v[0] + ", " + v[1]));
		}

		return fxTradeList;
	}


	private List<Map<String, String[]>> matchTradeField(List<Map<String, String[]>> fxBdtTradeList, String bdtFieldName,
			String swiftFieldValue) {
		String isMatched = "N";

		for (Map<String, String[]> fxTrade : fxBdtTradeList) {
			String[] bdtValues = fxTrade.get(bdtFieldName);
			String bdtValue = bdtValues[0];

			// matching logic per field goes here.
			if (bdtFieldName.equals("TP_FXSWLEG")) {
				isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
			} else if (bdtFieldName.equals("CNT_EVTL")) {
				// TODO: consider event mapping implementation here.
			} else if (bdtFieldName.equals("BLOCK_TRD")) {
				// do nothing. No MT300 in dump has this field.
			} else if (bdtFieldName.equals("TP_DVCS")) {
				if (bdtValue.equals("D")) {
					isMatched = swiftFieldValue.equals("N") ? "Y" : "N";
				} else if (bdtValue.equals("C")) {
					isMatched = swiftFieldValue.equals("Y") ? "Y" : "N";
				}
			} else if (bdtFieldName.equals("TP_DTETRN")) {
				isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
			} else if (bdtFieldName.equals("TP_PRICED")) {
				// swift has ',' formatted decimal while BDT has '.' formatted.
				float swiftExchPrice = Float.valueOf(swiftFieldValue.trim().replace(',', '.'));
				float bdtExchPrice = Float.valueOf(bdtValue.trim().replace(',', '.'));
				isMatched = swiftExchPrice == bdtExchPrice ? "Y" : "N";
			} else if (bdtFieldName.equals("TR_LOCA")) {
				isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
			} else if (bdtFieldName.equals("ISIN")) {
				isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
			} else if (bdtFieldName.equals("EXEC_MIC")) {
				isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
			} else if (bdtFieldName.equals("EXEC_TS")) {
				// swift val = 20201125084131/0000
				// BDT val = 2020-11-25T08:41:31.835404+00:00
				bdtValue = bdtValue.contains(".")
						? bdtValue.substring(0, bdtValue.indexOf(".")).replaceAll("-", "").replaceAll("T", "")
						: bdtValue;
				swiftFieldValue = swiftFieldValue.contains("/")
						? swiftFieldValue.substring(0, swiftFieldValue.indexOf("/")) : swiftFieldValue;
				isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
			} else if (bdtFieldName.equals("JURISDICT")) {
				if (bdtValue.contains(";") && swiftFieldValue.contains(";")) {
					List<String> bdtJuridictList = Arrays.asList(bdtValue.split(";"));
					List<String> swiftJuridictList = Arrays.asList(swiftFieldValue.split(";"));
					isMatched = bdtJuridictList.containsAll(swiftJuridictList) ? "Y" : "N";
				} else {
					isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
				}
			} else if (bdtFieldName.equals("UTI_NS")) {
				if (bdtValue.contains(";") && swiftFieldValue.contains(";")) {
					List<String> bdtJuridictList = Arrays.asList(bdtValue.split(";"));
					List<String> swiftJuridictList = Arrays.asList(swiftFieldValue.split(";"));
					isMatched = bdtJuridictList.containsAll(swiftJuridictList) ? "Y" : "N";
				} else {
					isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
				}
			} else if (bdtFieldName.equals("UTI_ID")) {
				if (bdtValue.contains(";") && swiftFieldValue.contains(";")) {
					List<String> bdtJuridictList = Arrays.asList(bdtValue.split(";"));
					List<String> swiftJuridictList = Arrays.asList(swiftFieldValue.split(";"));
					isMatched = bdtJuridictList.containsAll(swiftJuridictList) ? "Y" : "N";
				} else {
					isMatched = swiftFieldValue.equals(bdtValue) ? "Y" : "N";
				}
			}

			// filter out the field from matched BDT list if it is a mandatory
			// one to match
			if (mandatoryBDTMatch.contains(bdtFieldName) && isMatched.equals("N")) {
				fxBdtTradeList.remove(fxTrade);
			} else {
				fxTrade.put(bdtFieldName, new String[] { swiftFieldValue, isMatched });
			}
		}
		return fxBdtTradeList;
	}

}
