package com.validator.service;

import java.io.FileNotFoundException;
import java.util.List;

import com.validator.data.model.FileAudit;

public interface IValidatorService {

	List<FileAudit> findAll(String fileType);
	
	String[] loadBDTFiles(String fileType) throws FileNotFoundException;

	String[] loadSwiftFiles(String fileType) throws FileNotFoundException;

	String[] findInputFiles(String fileType) throws FileNotFoundException;

}
