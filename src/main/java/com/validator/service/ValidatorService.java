package com.validator.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.validator.data.model.FileAudit;
import com.validator.data.repository.EventFxRepository;
import com.validator.data.repository.EventMMRepository;
import com.validator.data.repository.FileRepository;
import com.validator.data.repository.FxSettlementRepository;
import com.validator.data.repository.IValidatorRepository;
import com.validator.data.repository.Swift300Repository;
import com.validator.data.repository.TradeFxRepository;
import com.validator.data.repository.TradeMMRepository;
import com.validator.matcher.MT300Matcher;

@Service
public class ValidatorService implements IValidatorService {

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private TradeFxRepository tradeFxRepository;

	@Autowired
	private TradeMMRepository tradeMMRepository;

	@Autowired
	private EventFxRepository eventFxRepository;

	@Autowired
	private EventMMRepository eventMMRepository;

	@Autowired
	private FxSettlementRepository settlementFxRepository;
	
	@Autowired
	private Swift300Repository swift300Repository;
	
	@Autowired
	private MT300Matcher mt300Matcher;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	// BDT input directories
	@Value("${bdt.base.filePath}")
	private String bdtBaseDir;

	@Value("${bdt.tradeFx.dir}")
	private String bdtTradeFxDir;

	@Value("${bdt.tradeMm.dir}")
	private String bdtTradeMmDir;

	@Value("${bdt.settlementFx.dir}")
	private String bdtSettlementFxDir;

	@Value("${bdt.settlementMm.dir}")
	private String bdtSettlementMmDir;

	@Value("${bdt.eventFx.dir}")
	private String bdtEventFxDir;

	@Value("${bdt.eventMm.dir}")
	private String bdtEventMmDir;

	// BDT file pattern
	@Value("${bdt.as.file}")
	private String bdtASFilePattern;

	@Value("${bdt.am.file}")
	private String bdtAMFilePattern;

	@Value("${bdt.eu.file}")
	private String bdtEUFilePattern;

	@Value("${conf.trd.fx.fields.index}")
	private String fxTradeIndex;

	@Value("${conf.trd.fx.fields}")
	private String fxTradeFields;

	@Value("${conf.settlement.fx.fields.index}")
	private String fxSettlementIndex;

	@Value("${conf.settlement.fx.fields}")
	private String fxSettlementFields;

	// BDT File types
	private final String TRADE_FX = "TradeFx";
	private final String TRADE_MM = "TradeMM";
	private final String SETTLEMENT_FX = "SettlementFx";
	private final String SETTLEMENT_MM = "SettlementMM";
	private final String EVENT_FX = "EventFx";
	private final String EVENT_MM = "EventMM";

	// Swift File types
	private final String MT103 = "MT103";
	private final String MT104 = "MT104";
	private final String MT202 = "MT202";
	private final String MT210 = "MT210";
	private final String MT298 = "MT298";
	private final String MT300 = "MT300";
	private final String MT320 = "MT320";
	private final String MT330 = "MT330";

	// Swift input directories
	@Value("${swift.base.filePath}")
	private String swiftBaseDir;

	@Value("${swift.103.dir}")
	private String mt103Dir;

	@Value("${swift.104.dir}")
	private String mt104Dir;

	@Value("${swift.202.dir}")
	private String mt202Dir;

	@Value("${swift.210.dir}")
	private String mt210Dir;

	@Value("${swift.298.dir}")
	private String mt298Dir;

	@Value("${swift.300.dir}")
	private String mt300Dir;

	@Value("${swift.320.dir}")
	private String mt320Dir;

	@Value("${swift.330.dir}")
	private String mt330Dir;

	@Value("${swift.sender.pattern}")
	String senderPatternString;

	@Value("${swift.receiver.pattern}")
	String receiverPatternString;

	@Override
	public List<FileAudit> findAll(String fileType) {
		return fileRepository.findByFileType(fileType).get();
	}

	@Override
	public String[] loadBDTFiles(String fileType) throws FileNotFoundException {
		return loadBDTFilesByType(fileType);
	}

	private String[] loadBDTFile(String fileType, String filePath, String mappedIndices, String mappedFields,
			IValidatorRepository dataRepository) throws FileNotFoundException {
		File dir = new File(filePath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			throw new FileNotFoundException("BDT Directory: " + filePath + " is empty");
		}
		for (File file : files) {
			long fileId = loadFileAudit(fileType, file.getName());
			Scanner scanner = new Scanner(file);
			boolean isHeader = true;
			while (scanner.hasNextLine()) {
				if (isHeader) {
					isHeader = false;
					scanner.nextLine();
					continue;
				}
				String bodyString = scanner.nextLine();
				String[] fields = bodyString.split("\\|");
				logger.info(fields.toString());
				Map<String, Object> tradeMap = new HashMap<String, Object>();
				tradeMap.put("file_id", fileId);
				tradeMap.put("created", new Date());
				String[] indices = mappedIndices.split(",");
				String[] fxFields = mappedFields.split(",");
				for (int i = 0; i < indices.length; i++) {
					int index = Integer.valueOf(indices[i]);
					String fieldName = fxFields[i];
					String fieldValue = fields[index - 1];
					tradeMap.put(fieldName, fieldValue);
				}
				dataRepository.insert(tradeMap);
			}
			logger.info("-End--------------------");
			scanner.close();
		}
		return dir.list();
	}

	private String[] loadBDTFilesByType(String fileType) throws FileNotFoundException {
		if (fileType.equalsIgnoreCase(TRADE_FX)) {
			String filePath = bdtBaseDir + "/" + bdtTradeFxDir;
			return loadBDTFile(fileType, filePath, fxTradeIndex, fxTradeFields, tradeFxRepository);
		} else if (fileType.equalsIgnoreCase(TRADE_MM)) {
			String filePath = bdtBaseDir + "/" + bdtTradeMmDir;
			return null;
			// TODO - implement TradeMM insert
		} else if (fileType.equalsIgnoreCase(SETTLEMENT_FX)) {
			String filePath = bdtBaseDir + "/" + bdtSettlementFxDir;
			return loadBDTFile(fileType, filePath, fxSettlementIndex, fxSettlementFields, settlementFxRepository);
		} else if (fileType.equalsIgnoreCase(SETTLEMENT_MM)) {
			String filePath = bdtBaseDir + "/" + bdtSettlementMmDir;
			return null;
			// TODO - implement SettlementMM insert
		} else if (fileType.equalsIgnoreCase(EVENT_FX)) {
			String filePath = bdtBaseDir + "/" + bdtEventFxDir;
			return null;
			// TODO - implement EventFx insert
		} else if (fileType.equalsIgnoreCase(EVENT_MM)) {
			String filePath = bdtBaseDir + "/" + bdtEventMmDir;
			return null;
			// TODO - implement EventMM insert
		} else {
			return null;
		}
	}

	private long loadFileAudit(String fileType, String fileName) {
		if (fileName == null || fileName.length() == 0) {
			return -1;
		}
		String fileDate = fileName.substring(0, 8);
		String region = "";
		if (fileName.contains(bdtASFilePattern)) {
			region = "AS";
		} else if (fileName.contains(bdtAMFilePattern)) {
			region = "AM";
		} else if (fileName.contains(bdtEUFilePattern)) {
			region = "EU";
		}
		fileRepository.insert(new FileAudit(fileName, fileDate, fileType, region));
		return fileRepository.getMaxId();
	}

	@Override
	public String[] loadSwiftFiles(String fileType) throws FileNotFoundException {
		return loadSwiftFilesByType(fileType);
	}

	private String[] loadSwiftFilesByType(String fileType) throws FileNotFoundException {
		if (fileType.equalsIgnoreCase(MT103)) {
			String filePath = swiftBaseDir + "/" + mt103Dir;
			return null;
			// TODO - implement
		} else if (fileType.equalsIgnoreCase(MT104)) {
			String filePath = swiftBaseDir + "/" + mt104Dir;
			return null;
			// TODO - implement
		} else if (fileType.equalsIgnoreCase(MT202)) {
			String filePath = swiftBaseDir + "/" + mt202Dir;
			return null;
			// TODO - implement
		} else if (fileType.equalsIgnoreCase(MT210)) {
			String filePath = swiftBaseDir + "/" + mt210Dir;
			return null;
			// TODO - implement
		} else if (fileType.equalsIgnoreCase(MT298)) {
			String filePath = swiftBaseDir + "/" + mt298Dir;
			return null;
			// TODO - implement
		} else if (fileType.equalsIgnoreCase(MT300)) {
			String filePath = swiftBaseDir + "/" + mt300Dir;
			return loadMT300File(fileType, filePath, tradeFxRepository);
		} else if (fileType.equalsIgnoreCase(MT320)) {
			String filePath = swiftBaseDir + "/" + mt320Dir;
			return null;
			// TODO - implement
		} else if (fileType.equalsIgnoreCase(MT330)) {
			String filePath = swiftBaseDir + "/" + mt330Dir;
			return null;
			// TODO - implement
		} else {
			return null;
		}
	}

	private String[] loadMT300File(String fileType, String filePath, TradeFxRepository tradeFxRepository2) throws FileNotFoundException {

		File dir = new File(filePath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			throw new FileNotFoundException("Swift Directory: " + filePath + " is empty");
		}
		for (File file : files) {
			long fileId = loadFileAudit(fileType, file.getName());
			Scanner scanner = new Scanner(file);
			
			String sequencePattern = ":[0-9A-Z]++:$";
			String fieldPattern = ":[0-9A-Z]++:.++";
			
			Pattern seqPattern =   Pattern.compile(sequencePattern);
			Pattern fldPattern =   Pattern.compile(fieldPattern);
			
			String swiftSender = "", swiftReceiver = "";
			
			String swiftField="", swiftSequence="";
			String prevSwiftFldValue="";
			
			Map<String, String> swiftMap = new HashMap<String, String>();
			
			while (scanner.hasNextLine()) {
				String bodyString = scanner.nextLine().trim();
				String swiftFldValue="";
				
				Matcher seqMatcher =   seqPattern.matcher(bodyString);
				Matcher fldMatcher =   fldPattern.matcher(bodyString);
				
				if(bodyString.equals("-}")){
					break;
				}
				
				if (bodyString.contains("}{")) {
					swiftSender = bodyString.substring(bodyString.indexOf("{1:")+3,bodyString.indexOf("}{2"));
					swiftReceiver = bodyString.substring(bodyString.indexOf("{2:")+3,bodyString.indexOf("}{3"));
					
					swiftMap.put("Sender", swiftSender);
					swiftMap.put("Receiver", swiftReceiver);
					continue;
				} else if (seqMatcher.matches()) {
					swiftSequence = seqMatcher.group().replaceAll(":", "");
					continue;
				} else if (fldMatcher.find()) {
					swiftField = bodyString.substring(bodyString.indexOf(":"), bodyString.lastIndexOf(":")+1);
					if (swiftSequence.startsWith("15B") && swiftField.equals(":32B:")) {
						swiftSequence = "15B_B1";
					} else if (swiftSequence.startsWith("15B") && swiftField.equals(":33B:")) {
						swiftSequence = "15B_B2";
					} else if (swiftSequence.startsWith("15E") && (swiftField.equals(":22L:") || swiftField.equals(":91A:")
							|| swiftField.equals(":91D:") || swiftField.equals(":91J:"))) {
						swiftSequence = "15E_E1";
					} else if (swiftSequence.startsWith("15E") && (swiftField.equals(":22M:") || swiftField.equals(":22N:"))) {
						swiftSequence = "15E_E1a";
					} else if (swiftSequence.startsWith("15E") && (swiftField.equals(":22P:") || swiftField.equals(":22R:"))) {
						swiftSequence = "15E_E1aA1";
					} else if(swiftSequence.startsWith("15E") && !(swiftField.equals(":22L:") || swiftField.equals(":91A:")
							|| swiftField.equals(":91D:") || swiftField.equals(":91J:") || swiftField.equals(":22M:") || swiftField.equals(":22N:") || swiftField.equals(":22P:") || swiftField.equals(":22R:"))){
						swiftSequence = "15E";
					}
					swiftField = swiftField.replaceAll(":", "");
					swiftFldValue = bodyString.substring(bodyString.lastIndexOf(":")+1);
					prevSwiftFldValue=swiftFldValue;
					
					if (swiftMap.get(swiftSequence + "_F_" + swiftField)==null) {
						swiftMap.put(swiftSequence + "_F_" + swiftField, swiftFldValue);
					}else{
						swiftMap.put(swiftSequence + "_F_" + swiftField, swiftMap.get(swiftSequence + "_F_" + swiftField)+";"+swiftFldValue);
					}
				} else {
					swiftFldValue = prevSwiftFldValue+"|"+bodyString;
					prevSwiftFldValue=swiftFldValue;
					
					swiftMap.put(swiftSequence + "_F_" + swiftField, swiftFldValue);
				}
			}
			scanner.close();
			swiftMap.forEach((k, v) -> System.out.println(k + " = " + v));
			
			if(swiftSender.length()<1 || swiftReceiver.length()<1){
				logger.error("MT file can not be processed as Sender or Receiver fields are not read");
				throw new RuntimeException("MT file can not be processed as Sender or Receiver fields are not read");
			}
						
			swift300Repository.insert(fileId, swiftMap);
			logger.info("-End--------------------");
			
			
				mt300Matcher.match(swiftMap, file.getName());
			
		}
		
		return dir.list();
	}

	@Override
	public String[] findInputFiles(String fileType) throws FileNotFoundException {
		String filePath ="";
		if (fileType.equalsIgnoreCase(TRADE_FX)) {
			filePath = bdtBaseDir + "/" + bdtTradeFxDir;
		} else if (fileType.equalsIgnoreCase(TRADE_MM)) {
			filePath = bdtBaseDir + "/" + bdtTradeMmDir;
		} else if (fileType.equalsIgnoreCase(SETTLEMENT_FX)) {
			filePath = bdtBaseDir + "/" + bdtSettlementFxDir;
		} else if (fileType.equalsIgnoreCase(SETTLEMENT_MM)) {
			filePath = bdtBaseDir + "/" + bdtSettlementMmDir;
		} else if (fileType.equalsIgnoreCase(EVENT_FX)) {
			filePath = bdtBaseDir + "/" + bdtEventFxDir;
		} else if (fileType.equalsIgnoreCase(EVENT_MM)) {
			filePath = bdtBaseDir + "/" + bdtEventMmDir;
		} else if (fileType.equalsIgnoreCase(MT300)) {
			filePath = swiftBaseDir + "/" + mt300Dir;
		} else if (fileType.equalsIgnoreCase(MT320)) {
			filePath = swiftBaseDir + "/" + mt320Dir;
		} else if (fileType.equalsIgnoreCase(MT330)) {
			filePath = swiftBaseDir + "/" + mt330Dir;
		}
		File dir = new File(filePath);
		return dir.list();
	}

	
}
